.. highlight:: shell

============
Update
============

To update the NavARP package first uninstall it::

    pip uninstall navarp

Then run the following command for the last stable version::

    pip install navarp

Or for the last version still under development run::

    pip install https://gitlab.com/fbisti/navarp/-/archive/develop/navarp-develop.zip
