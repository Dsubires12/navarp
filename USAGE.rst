.. highlight:: shell

============
Usage
============

The following documentation is covering only the basic commands in the NavARP GUI (``navarp.py``).
The independent usage of the ``navarp.utils`` libraries is instead reported as Jupyter notebooks in the ``example`` folder.

Loading data
-------------------------------

NavARP can open the following data types:

* NXarpes file from LOREA/ALBA(ES) and I05/Diamond(GB);
* HDF5 file from SXARPES-ADRESS/PSI(CH);
* NEXUS file from Antares/Soleil(FR) (only deflector scan);
* folder with txt-files from Cassiopee/Soleil(FR);
* txt-file from MBS A1Soft program;
* zip- or txt-file from Scienta-Omicron SES program;
* sp2 file from Specs program;
* pxt, ibw and itx file of Igor-pro as saved by Scienta-Omicron SES program;
* txt file in itx format of Igor-pro considering the order (energy, angle, scan);
* yaml file with dictionary to load files (txt, sp2 or pxt) in a folder or to only add metadata to a single file.

To open the file click Open in the File-Menu on the top left and select the single file or, in the case collection of txt-files from Cassiopee/Soleil(FR), select a ROI-file inside the folder.
To open a folder with a collection of txt-files from MBS A1Soft, sp2-files from Specs program, pxt- or txt-files from Scienta-Omicron SES program, write first a yaml-file inside that folder with the instruction for opening the file.
If for example the folder contains "file_name_001.txt, file_name_002.txt, file_name_003.txt, etc." the related yaml-file inside that folder (called for example ``file_name.yaml``) can be::

    # ----------------------------------------------------------------
    # Required parameters
    # ----------------------------------------------------------------
    
    # file_path, the * must to be exactly where the variable number is
    file_path: 'file_name_*.txt'
    
    # scans can be start and step (as below), or start and stop (replace step with stop)
    # in the case below, it starts from 20 with a step of 0.5
    scans:
      start: 20
      step: 0.5
    
    # scan_type can be 'tilt', 'polar', 'azimuth', 'deflector' or 'hv'
    scan_type: 'azimuth'
    
    
    # ----------------------------------------------------------------
    # Optional parameters
    # ----------------------------------------------------------------
    
    # photon energy, it can be specified and other value from the files will be discarded
    hv: 60
    
    # analyzer, this define the analyzer geometry, if not specified default values will be used
    analyzer:
      tht_ap: 50
      phi_ap: 0
      work_fun: 4.5
      deflector: False


Navigate through the data
-------------------------------
To navigate the data use the **Navigation panel** at the top right or **mouse right-click** on the top (bottom) graph to change the energy (scan) value.

The mouse action can be only a single right-click-and-release in a particular region or the right-click can be kept pressed for a smooth movement to a final release point.

**Important**, the right-click mouse navigation works properly if the "pan/zoom" or "zoom rect" are not selected in the navigation toolbar at the bottom of the figure (the GUI start with neither of the two options selected).

With the mouse cursor on top of a graph (either top or bottom), use the **mouse scroll** to change the integration region for the iso-value (energy or angle), each scroll step is doubling or halving such region.

Colormap scale
-------------------------------
Color scale setting can be modified in the **Color scale parameters** tab on the right (click on the tab name if not already selected).

Fermi level
-------------------------------
Fermi level can be determined in the **Fermi level alignment** tab (click on the tab name if not already selected).

The "**No Fermi level alignment**" option keep the data in kinetic energy (:math:`E_{kin}`) or, in the case of photon energy scan, in binding energy (:math:`E_{bin}`) as obtained from :math:`E_{bin} = E_{kin} - (h\nu - \Phi)`, where :math:`h\nu` is the photon energy and :math:`\Phi` is the analyzer work function. 

"**Use Fermi level at**" uses the Fermi level inserted in the box below. "**Find Fermi level using**" looks for the Fermi level following the set-up selected below where: "Energy range" can be all the available one (selecting the radio button on **full**) or within the horizontal lines in the top panel (selecting **cursor** instead); the Fermi level can be the same for all the scan image (selecting "Scan value" **all**) or different values for each scan image, option particularly useful in photon energy scan ("Scan value" radio button on **each**).

Transformation in the k-space
-------------------------------
In the **k-space transformation** tab it is possible to set up all the parameters used for the transformation from angle to momentum scale (the method is based on the kinetic energy of the electrons and so it is independent from the Fermi level alignment).

The **from cursor** button auto-fills the "Point in angles" with the actual cursor position in the figure.

The **set** :math:`\Gamma` just put zeros in :math:`k_x` and :math:`k_y`. The inner potential (:math:`V0`) is 10 by default, it is used only for the photon energy scan and so it must be properly modified only in that case. Use :math:`photons = yes` to include the photon momentum for the determination of the initial electron momentum. In this case, the "Analyzer" parameters must be properly filled (**important**, at the present version, the "Analyzer" parameters, and so the photon momentum, are correctly defined only for beamlines LOREA/ALBA(ES) and SXARPES-ADRESS/PSI(CH)).
Once everything is properly set, it is possible to click the Iso-E (k) blue button in the "Plot mode".

File information
-------------------------------
**File information** tab is the selected one by default after starting the GUI and it is reporting the information extracted from the data file.
