Changelog
==========

1.2.0: 2021/11/23
------------------
This version includes many fix, a revision of the documentations and the following new features: (i) support to the new MBS-krx file the lorea NxARPES-file; (ii) the minimum gradient method as post-processing procedure.
Below a list of the main changes divided by modules:

* `navarp_gui.py`:

  * Added post-processing procedure with order=1, which is the min minimum gradient method.

* `utils.navfile`:

  * Fixed I05 file loading;
  * Added new MBS-krx 64 bit file keeping the compatibility with the 32 bit one;
  * Added tht_an and kspace in the yaml-file for loading already transformed data.

* `utils.isomclass`:

  * Transpose data in isok export as nexus.


1.1.0: 2021/04/11
------------------

This version includes very interesting new features as: (i) exporting the analyzed data on top or bottom panels as NXdata, or Igor Pro Text file, or HD5F; (ii) post-processing procedures as second derivative (in one direction), Laplace filter (if both direction are considered), curvature (only in one direction at the moment), Gaussian filter. Below a list of the main changes divided by modules:

* `navarp_gui.py`:

  * Added post-processing procedure;
  * Added panel for exporting the data;
  * Use isomclass when possible;
  * Removed the boolean variable in the analyzer panel called deflector because useless.
  * Write efermi in the gui line edit when file loaded.

* `utils.navfile`:

  * Removed the last angle value if angle array has the same length of energies;
  * Fixed I05 file loading;
  * Removed the boolean variable in NavAnalyzer called deflector because useless.

* `utils.isomclass` (new file):

  * Fix isoenergy export as itx by transposing data.
  * Fix export as NXdata and Igor-pro text file.
  * Add postprocessing function and use it in all the isomclass.
  * Use laplace filter if sigma has two values and use curvature only for sigma with one value.
  * Rename file_path_* as file_path in all the export_as_* of the isomclass.

* `utils.kinterp`:

  * In get_isoscan, use np.nan in fill_value instead of extrapolate because it is not correct to extrapolate data.

* INTALLATION.rst:

  * The installation instruction of the master branch are now based on the PyPI package.

* UPDATE.rst:

  * The update instruction of the master branch are now based on the PyPI package.

1.0.0: 2021/04/11
------------------

This is one of the most important release of NavARP, and the project is approaching to a more stable stage. This new version adds very important methods to the NavEntry class which are nicely shown in the now-available examples gallery! In addition, the loading speed has been improved for many text-based files input, and now the krx-MBS format is also supported. Below a list of the main changes divided by modules:

* `navarp_gui.py`:

  * Added more colorscale maps;
  * After closing the program, its window size and position are saved (as a file called .navarp inside the user home directory) and they will be used to restore the program the next time it will be opened;

* `utils.navfile`:

  * Added support for krx-MBS file format;
  * Added support for ibw file format as saved by Scienta-Omicron SES program;
  * Added support for txt-Scienta file format with 3 dimensions;
  * Added support for igor-pro text file format (saved as .itx or .txt);
  * Loading speed of about x3 faster for txt-based file input (txt from Scienta and MBS, sp2 from Specs);
  * Added set_efermi method to NavEntry to set the efermi of the entry object;
  * Added autoset_efermi method to NavEntry to automatically found the efermi by curve fitting;
  * Added plt_efermi_fit method to NavEntry to show the autoset_efermi results;
  * Added set_tht_an method to NavEntry to set the tht_an angle of the the entry object for k-space tranformation;
  * Added set_kspace method to NavEntry to set tht_an, phi_an, scans_0 and phi used in the k-space tranformation;
  * Added isoscan, isoenergy, isoangle and isok methods in the NavEntry calling the respectively classes in isomclass.
  * Extended dictionary in yaml-file, if loaded overwrite any attribute and efermi and tht_an can be also directly assigned.

* `utils.isomclass` (new file):

  * Added IsoScan class with the methods to show in a plot, export as NXdata or Igor-pro text file, postprocessing as interpolation, second derivative and curvature;
  * Added IsoEnergy class  with the methods to show in a plot, export as NXdata or Igor-pro text file, postprocessing as interpolation, second derivative and curvature;
  * Added IsoAngle class with the methods to show in a plot, postprocessing as second derivative and curvature;
  * Added IsoK class with the methods to show in a plot, export as NXdata or Igor-pro text file, postprocessing as second derivative and curvature;

* `utils.fermilevel`:

  * Added fit procedure without using lmfit, lmfit is no longer required in navarp;
  * Removed all the previous functions based on lmfit.

* `utils.ktransf`:

  * Removed all the deprecated functions for k-space transformation.

* `examples directory`:

  * The old example directory now is called examples and contains the python script to get the gallery.

* `extras.simulation`:

  * Added functions to simulate the graphene band structure probed by deflector and hv scans.

* `requirements.txt and setup.py`:

  * Removed lmfit from the required packages.


0.18.0: 2020/04/11
------------------

This version gives the possibility to open four additional file formats.

* `navarp.py` (now renamed as `navarp_gui.py`):

  * Added click command;
  * Import navarp.utils, so now navarp requires installation with pip before usage;
  * Renamed as navarp_gui.py so to avoid importing conflicts.

* `utils.navfile`:

  * Added loading MBS A1Soft text-files;
  * Added case scan_type=deflector for Lorea by reading defl_angles.

* `setup.py`:

  * Added click command.


0.17.0: 2020/08/14
------------------

This version gives the possibility to open four additional file formats.

* `navarp.py`:

  * Added .navarp configuration file saved in the local user home
  * Set the initial default value of k-transf to be without photons contribution

* `utils.navfile`:

  * Added loading Scienta-Omicron SES zip and text files;
  * Added loading Specs Prodigy sp2 files;
  * Added loading Igor-pro pxt files as saved by Scienta-Omicron SES;
  * Added loading folder with txt, sp2 or pxt files using instructions in a yaml file.

* `setup.py`:

  * Improved requirements avoiding to install PyQt5 if in conda it is already present.


0.16.0: 2020/08/05
------------------

This version adds the sphinx-doc integrated in GitLab pages and utils.kinterp.

* `navarp.py`:

  * Added azimuth scan_type
  * Added arctan2 value for sample alignment
  * Fixed Antares data including the case of MBSAcquisition_1

* `utils`:

  * Added `kinterp`
